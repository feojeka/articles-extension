<?php

$installer = $this;
$installer->startSetup();

$installer->run("
DROP TABLE IF EXISTS `egolubev_articles`
");

$installer->run("
CREATE TABLE `{$this->getTable('egolubev_articles/article')}`
(
	`id` int(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    `title` varchar(255) NOT NULL,
    `contents` TEXT NOT NULL,
    `image` varchar(255),
    `active` BOOL default true
) ENGINE=InnoDB DEFAULT CHARSET=utf8
");

$installer->endSetup();