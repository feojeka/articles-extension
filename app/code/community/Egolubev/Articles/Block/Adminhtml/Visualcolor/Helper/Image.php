<?php

class Egolubev_Articles_Block_Adminhtml_Visualcolor_Helper_Image extends Varien_Data_Form_Element_Image
{
    protected function _getUrl()
    {
        $url = false;
        if ($this->getValue()) {
            $url = Mage::getBaseUrl('media').'articles/'.$this->getValue();
        }
        return $url;
    }

}
