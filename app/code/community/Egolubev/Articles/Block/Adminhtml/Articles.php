<?php

class Egolubev_Articles_Block_Adminhtml_Articles extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    protected function _construct()
    {
        $this->_addButtonLabel = Mage::helper('egolubev_articles')->__('Add new article');

        $this->_blockGroup = 'egolubev_articles';
        $this->_controller = 'adminhtml_articles';
        $this->_headerText = Mage::helper('egolubev_articles')->__('Articles');
    }
}