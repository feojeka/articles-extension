<?php

class Egolubev_Articles_Block_Adminhtml_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    protected function _construct()
    {
        $this->_blockGroup = 'egolubev_articles';
        $this->_mode = 'edit';
        $this->_controller = 'adminhtml';

        $article_id = (int)$this->getRequest()->getParam($this->_objectId);
        $article = Mage::getModel('egolubev_articles/article')->load($article_id);
        Mage::register('current_article', $article);
    }

    public function getHeaderText()
    {
        $article = Mage::registry('current_article');
        if ($article->getId()) {
            return Mage::helper('egolubev_articles')->__("Edit Article '%s'", $this->escapeHtml($article->getTitle()));
        } else {
            return Mage::helper('egolubev_articles')->__('Add new Article');
        }
    }
}