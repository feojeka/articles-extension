<?php

class Egolubev_Articles_Block_Adminhtml_Articles_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    protected function _construct()
    {
        $this->setId('articlesGrid');
        $this->_controller = 'adminhtml_articles';
        $this->setUseAjax(true);

        $this->setDefaultSort('id');
        $this->setDefaultDir('desc');
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('egolubev_articles/article')->getCollection();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header'       =>       Mage::helper('egolubev_articles')->__('ID'),
            'align'        =>       'center',
            'width'        =>       '20px',
            'filter_index' =>       'id',
            'index'        =>       'id'
        ));

        $this->addColumn('title', array(
            'header'        =>       Mage::helper('egolubev_articles')->__('Title'),
            'align'         =>       'left',
            'filter_index'  =>       'title',
            'index'         =>       'title',
            'type'          =>       'text',
            'truncate'      =>       50,
            'escape'        =>       true
        ));

        $this->addColumn('content', array(
            'header'        =>       Mage::helper('egolubev_articles')->__('Content'),
            'align'         =>       'left',
            'filter_index'  =>       'contents',
            'index'         =>       'contents',
            'type'          =>       'text',
            'truncate'      =>       200,
            'escape'        =>       true
        ));

        $this->addColumn('Active', array(
            'header'        =>       Mage::helper('egolubev_articles')->__('Active'),
            'width'         =>       '50px',
            'align'         =>       'center',
            'type'          =>       'checkbox',
            'index'         =>       'active',
            'values'        =>       array(1,2),
            'field_name'    =>       'is_active',
            'filter'        =>       false
        ));

        $this->addColumn('thumbnail', array(
            'header'        =>       Mage::helper('egolubev_articles')->__('Thumbnail'),
            'width'         =>       '50px',
            'align'         =>       'left',
            'index'         =>       'image',
            'frame_callback'=>       array($this, 'callback_image'),
            'filter'        =>       false,
            'sortable'      =>       false
        ));


        $this->addColumn('action', array(
            'header'        =>       Mage::helper('egolubev_articles')->__('Action'),
            'width'         =>       '50px',
            'type'          =>       'action',
            'getter'        =>       'getId',
            'actions'       =>       array(
                array(
                    'caption'   =>  Mage::helper('egolubev_articles')->__('Edit'),
                    'url'       =>  array(
                        'base'  =>  '*/*/edit',
                    ),
                    'field'     =>  'id'
                )
            ),
            'filter'        =>       false,
            'sortable'      =>       false,
            'index'         =>       'id'
        ));

        return parent::_prepareColumns();
    }

    public function callback_image($value)
    {
        $width = 70;
        $height = 70;

        return "<img src='".Mage::getBaseUrl('media').'articles/'.$value."' width=".$width." height=".$height."/>";
    }

    public function getRowUrl($article)
    {
        return $this->getUrl('*/*/edit', array(
            'id' => $article->getId(),
        ));
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

}