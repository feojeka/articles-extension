<?php

class Egolubev_Articles_Block_Adminhtml_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $action_name = $this->getRequest()->getActionName();
        $article = Mage::registry('current_article');
        $form = new Varien_Data_Form(array(
            'enctype' => 'multipart/form-data'
        ));

        $fieldset = $form->addFieldset('edit_article', array(
            'legend' => Mage::helper('egolubev_articles')->__('Article Details')
        ));

        if ($article->getId()) {
            $fieldset->addField('id', 'hidden', array(
                'name'      =>  'id',
                'required'  =>  true
            ));
        }

        $fieldset->addField('title', 'text', array(
            'name'      =>      'title',
            'label'     =>      Mage::helper('egolubev_articles')->__('Title'),
            'maxlength' =>      '250',
            'required'  =>      true
        ));

        $fieldset->addField('contents', 'textarea', array(
            'name'      =>      'contents',
            'label'     =>      Mage::helper('egolubev_articles')->__('Content'),
            'style'     =>      'width: 98%; height: 200px;',
            'required'  =>      true
        ));

        $fieldset->addType('image', 'Egolubev_Articles_Block_Adminhtml_Visualcolor_Helper_Image');

        $fieldset->addField('image', 'image', array(
            'label'     =>      Mage::helper('egolubev_articles')->__('Image'),
            'name'      =>      'imagefile',
            'value'     =>      'image',
            'required'  =>      false
        ));

        if ($action_name == 'edit') {
            $fieldset->addField('active', 'checkbox', array(
                'label'     =>      Mage::helper('egolubev_articles')->__('Active'),
                'name'      =>      'active',
                'checked'   =>      (int)$article->getActive() > 0 ? 'checked' : '',
                'onclick'   =>      'this.value = this.checked ? 1 : 0;'
            ));
        }

        if ($action_name == 'new') {
            $fieldset->addField('active', 'checkbox', array(
                'label'     =>      Mage::helper('egolubev_articles')->__('Active'),
                'name'      =>      'active',
                'value'     =>      1,
                'checked'   =>      true,
                'onclick'   =>      'this.value = this.checked ? 1 : 0;'
            ));
        }

        $form->setMethod('post');
        $form->setUseContainer(true);
        $form->setId('edit_form');
        $form->setAction($this->getUrl('*/*/save'));
        $form->setValues($article->getData());

        $this->setForm($form);
    }
}