<?php

class Egolubev_Articles_Adminhtml_ArticlesController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->_title($this->__('Articles'));

        $this->loadLayout();
        $this->_setActiveMenu('egolubev_articles');
        $this->_addBreadcrumb(Mage::helper('egolubev_articles')->__('Articles'), Mage::helper('egolubev_articles')->__('Articles'));
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_title($this->__('Add new article'));

        $this->loadLayout();
        $this->_setActiveMenu('egolubev_articles');
        $this->_addBreadcrumb(Mage::helper('egolubev_articles')->__('Add new article'), Mage::helper('egolubev_articles')->__('Add new article'));
        $this->renderLayout();
    }

    public function editAction()
    {
        $this->_title($this->__('Edit article'));

        $this->loadLayout();
        $this->_setActiveMenu('egolubev_articles');
        $this->_addBreadcrumb(Mage::helper('egolubev_articles')->__('Edit article'), Mage::helper('egolubev_articles')->__('Edit Article'));
        $this->renderLayout();
    }

    public function deleteAction()
    {
        $articleId = $this->getRequest()->getParam('id', false);
        try {
            Mage::getModel('egolubev_articles/article')->setId($articleId)->delete();
            Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('egolubev_articles')->__('Article successfully deleted'));
            return $this->_redirect('*/*/');
        } catch (Mage_Core_Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        } catch (Exception $e) {
            Mage::logException($e);
            Mage::getSingleton('adminhtml/session')->addError($this->__('Something went wrong'));
        }

        $this->_redirectReferer();
    }

    public function saveAction()
    {
        $data = $this->getRequest()->getPost();
        if(!empty($data)) {
            try {
                if (isset($_FILES['imagefile']['name']) && $_FILES['imagefile']['name'] != '') {
                    $imageFileName = time() . '_' . $_FILES['imagefile']['name'];
                    $uploader = new Varien_File_Uploader($_FILES['imagefile']);
                    $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
                    $path = Mage::getBaseDir('media') . DS . 'articles' . DS;
                    $uploader->save($path, $imageFileName);
                    $data['image'] = $imageFileName;
                }
                else if ((isset($data['imagefile']['delete']) && $data['imagefile']['delete'] == 1)) {
                    $data['image'] = '';
                    unlink(Mage::getBaseDir('media') . DS . 'articles' . DS . $data['imagefile']['value']);
                }
                $data['active'] = isset($data['active']) ? 1 : 0;
                Mage::getModel('egolubev_articles/article')->setData($data)->save();
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('egolubev_articles')->__('Article successfully saved'));
            } catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::logException($e);
                Mage::getSingleton('adminhtml/session')->addError($this->__('Something went wrong'));
            }
        }

        return $this->_redirect('*/*/');
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('egolubev_articles/adminhtml_articles_grid')->toHtml()
        );
    }
}