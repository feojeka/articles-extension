<?php

class Egolubev_Articles_Model_Article extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('egolubev_articles/article');
    }
}
