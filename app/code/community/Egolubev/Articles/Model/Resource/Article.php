<?php

class Egolubev_Articles_Model_Resource_Article extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init('egolubev_articles/article', 'id');
    }
}